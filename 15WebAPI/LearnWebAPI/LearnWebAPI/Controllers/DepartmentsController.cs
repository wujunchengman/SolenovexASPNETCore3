﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LearnWebAPI.Models;
using LearnWebAPI.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace LearnWebAPI.Controllers
{
    [Microsoft.AspNetCore.Components.Route("v1/[controller]")]
    [ApiController]
    public class DepartmentsController:ControllerBase
    {
        private readonly IDepartmentRepository _departmentRepository;

        public DepartmentsController(IDepartmentRepository departmentRepository)
        {
            _departmentRepository = departmentRepository;
        }

        [HttpGet] //v1/Departments verb: Get
        public async Task<ActionResult<IEnumerable<Department>>> GetAll()
        {
            var departments = await _departmentRepository.GetAll();
            if (!departments.Any())
            {
                return NoContent();
            }

            return Ok(departments);
            // return new ObjectResult(deparments);
        }

        [HttpPost]
        public async Task<ActionResult<Department>> Add([FromBody]Department department)
        {
            var added = await _departmentRepository.Add(department);
            return Ok(added);
        }
    }
}