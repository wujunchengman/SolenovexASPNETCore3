﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LearnWebAPI.Models;

namespace LearnWebAPI.Repositories
{
    public interface IEmployeeRepository
    {
        Task Add(Employee employee);
        Task<IEnumerable<Employee>> GetByDepartmentId(int departmentId);
        Task<Employee> Fire(int id);
    }
}