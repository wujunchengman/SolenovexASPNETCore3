﻿using Microsoft.AspNetCore.SignalR;
using SignalRDemo.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SignalRDemo
{
    public class CountHub:Hub
    {
        private readonly CountService _countService;

        public CountHub(CountService countService)
        {
            _countService = countService;
        }

        public async Task GetLatestCount()
        {
            int count;
            do
            {
                count = _countService.GetLatestCount();

                Thread.Sleep(1000);
            }
        }
    }
}
