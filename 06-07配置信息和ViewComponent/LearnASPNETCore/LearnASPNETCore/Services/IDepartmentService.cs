﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using LearnASPNETCore.Models;


namespace LearnASPNETCore.Services
{
    public interface IDepartmentService
    {
        /// <summary>
        /// 获取所有部门
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Department>> GetAll();

        /// <summary>
        /// 通过id获取部门
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Department> GetById(int id);

        /// <summary>
        /// 获得公司总体情况
        /// </summary>
        /// <returns></returns>
        Task<CompanySummary> GetCompanySummary();
        /// <summary>
        /// 添加部门
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        Task Add(Department department);
    }
}