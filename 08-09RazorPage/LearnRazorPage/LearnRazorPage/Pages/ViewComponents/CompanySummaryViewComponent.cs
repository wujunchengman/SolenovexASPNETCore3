﻿using System.Threading.Tasks;
using LearnRazorPage.Services;
using Microsoft.AspNetCore.Mvc;

namespace LearnRazorPage.Pages.ViewComponents
{
    //CROR R+R重构（批量修改变量、方法名）
    public class CompanySummaryViewComponent:ViewComponent
    {
        private readonly IDepartmentService _departmentService;

        public CompanySummaryViewComponent(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }
        public async Task<IViewComponentResult> InvokeAsync(string title)
        {
            ViewBag.Title = title;
            var summary = await _departmentService.GetCompanySummary();
            return View(summary);
        }
    }
}