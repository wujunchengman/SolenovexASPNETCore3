﻿namespace LearnASPNETCore.Models
{
    public class Employee
    {
        //使用prop代码片段可以快速生成字段
        //prop + 两次tab
        public int Id { get; set; }
        public int DepartmentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public bool Fired { get; set; }
    }

    public enum Gender
    {
        女 = 0,
        男 = 1
    }
}