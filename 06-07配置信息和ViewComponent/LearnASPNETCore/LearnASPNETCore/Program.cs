using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace LearnASPNETCore
{
    public class Program
    {
        //ASP.NET Core本质上是一个控制台项目
        public static void Main(string[] args)
        {
            //配置ASP.Net Core项目并运行
            CreateHostBuilder(args).Build().Run();
        }

        //配置ASP.Net Core的方法
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)

                //使用自定义的json配置文件
                .ConfigureAppConfiguration((context, configBuilder) =>
                {
                    //清理原来的配置文件信息
                    configBuilder.Sources.Clear();
                    //添加自定义的json配置信息
                    configBuilder.AddJsonFile("nick.json");
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    //默认加载appsettings.json配置文件

                    webBuilder.UseStartup<Startup>();

                    //如果修改Startup针对不同的环境执行不同的配置，需要修改为如下形式
                    //webBuilder.UseStartup(typeof(Program));
                    //typeof(Program)是一个程序集
                });
    }
}
