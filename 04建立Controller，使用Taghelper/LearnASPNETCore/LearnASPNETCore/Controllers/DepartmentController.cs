﻿
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LearnASPNETCore.Models;
using LearnASPNETCore.Services;

namespace LearnASPNETCore.Controllers
{
    public class DepartmentController : Controller
    {
        private readonly IDepartmentService _departmentService;
        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        //建立Action
        public async Task<IActionResult> Index()
        {
            ViewBag.Title = "Department Index";
            var departments = await _departmentService.GetAll();

            //特别注意，在VS2019中文版中，Views->Department建立"Index" Razor页面时并不像老师的那样页面是一个空页面
            //它会自动指定model模型，但是这个自动指定的模型并不是这个参数的模型，会导致报错，需要修改
            //P5的时候老师会讲
            return View(departments);


        }

        //作用：跳转到添加页面
        [HttpGet]
        public IActionResult Add()
        {
            ViewBag.Title = "Add Department";

            //先new一个model传递给View，然后可以在View中添加属性，然后再提交
            return View(new Department());
        }

        //默认访问时以Get方式访问，会执行上方Get方式的Add，此时return View页面返回给浏览器，并传递一个空模型
        //当用户点击submit按钮后，表单form的action依然是Add，但是form的请求方式为Post，所以会选择下方Post方式的Add，将数据保存到Model并重定向到Index

        //表单提交方式为Post
        [HttpPost] //C#特性
        public async Task<IActionResult> Add(Department model)//model形参由View提交过来
        {
            if (ModelState.IsValid)
            {
                await _departmentService.Add(model);
            }

            //使用nameof()利用重构重命名，在需要修改时IDE可以查找到相同变量批量修改
            return RedirectToAction(nameof(Index));
        }
    }
}
