# ASP.NET Core MVC中URL与Control和Action的关系

在路由终结点中间件中，会定义路由模板，具体专业描述我还不是太清楚，这里只是对这个配置代表的内容做一个补充，以便后面更好的理解

---

```csharp
"{controller=Department}/{action=Index}/{id?}"
```

这一行代码在Startup中的路由终结点中间件中配置

我要说的就是这一行代码，这一行定义了从浏览器访问一个URL，ASP.NET Core MVC收到HTTP请求时会进行怎样解析，明白了这个，对MVC的理解非常有帮助



MVC：**Model - View - Controller**

我们假设路由模板是这样的：

```csharp
"{controller=Department}/{action=Index}/{id?}"
```

**MVC中的URL对应关系：**主机地址或域名 : 端口号 / Controller / Action / 参数（可选）

示例1：http://localhost:5000/Department/Index

当浏览器访问这个URL时，会执行DepartmentController类中的Index方法，并且不会向View中传递参数

示例2：http://localhost:5000

当浏览器访问这个URL时，因为之前的路由模板，在没有Controller时会默认指定Department，在没有Action时会默认指定Index，所以示例1和示例2都是执行DepartmentController类中的Index方法

示例3：http://localhost:5000/Department/Index/3

当浏览器访问这个URL时，会执行DepartmentController类中的Index方法，并且还会向View传递Id参数，在View页面中可以接收这个参数进行操作，如修改对应Id的数据