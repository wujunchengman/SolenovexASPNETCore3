using LearnASPNETCore.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;


namespace LearnASPNETCore
{
    //Startup支持针对不同的运行环境执行不同的配置，如果使用不同的配置需要修改Program
    public class Startup
    {
        private readonly IConfiguration _configuration;

        //通过构造函数进行配置文件依赖注入
        //安装ReSharper后可以使用ALT + ENTER快捷键添加私有字段和初始化语句
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
            
        }


        //依赖注入配置支持针对不同的运行环境执行不同的注入
        public void ConfigureServicesDevelopment(IServiceCollection services) { }

        //主要负责配置依赖注入（依赖注入部分可以看一下我的博客）
        public void ConfigureServices(IServiceCollection services)
        {
            //注册Controller和View，但不包括Razor Pages
            services.AddControllersWithViews();

            //如果不需要Views可以使用这个方法只注册Controller
            //services.AddControllers();

            //如果需要Razor Pages可以注册这个服务
            //services.AddRazorPages();

            //注册自己的服务
            //注册生命周期为单例模式，泛型中前面是接口，后面是具体的类
            //每当其他类型请求一个实现了IClock接口的对象时，返回ChinaClock类的实例
            services.AddSingleton<IClock, ChinaClock>();

            services.AddSingleton<IDepartmentService, DepartmentService>();
            services.AddSingleton<IEmployeeService, EmployeeService>();

            //将appsetting.json的LearnASPNETCore配置信息映射到LearnASPNETCoreOptions类中
            services.Configure<LearnASPNETCoreOptions>(_configuration.GetSection("LearnASPNETCore"));
        }


        //ASP.NET Core支持针对不同的环境配置HTTP请求管道
        //针对Development环境执行如下HTTP请求管道配置
        //public void ConfigureDevelopment(IApplicationBuilder app, IWebHostEnvironment env){ }


        //此方法由运行时调用。使用此方法配置HTTP请求管道。
        //管道中处理请求的叫做中间件
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //判断是否是开发模式
            //在项目属性->调试->环境变量 中设置
            //IsDevelopment、IsStaging、IsProduction是内置的判断方法
            //判断自定义的环境变量：IsEnvironment("自定义环境变量")
            if (env.IsDevelopment())
            {
                //发生未被处理的异常，将错误信息展示到一个页面内，页面内包含错误的详细信息
                app.UseDeveloperExceptionPage();
            }

            /* 中间件注册的顺序十分重要
             * 中间件注册的顺序就是HTTP请求在管道中经过这些中间件处理的顺序
             */

            //静态文件中间件,不适用则客户端无法访问静态文件（html、css、js等）
            //所有静态文件都应该放在wwwroot文件夹
            app.UseStaticFiles();

            //可以将HTTP请求转换为HTTPS请求，强迫用户使用TLS协议
            app.UseHttpsRedirection();

            //身份认证中间件,必须放在端点中间件之前
            //因为通常是需要在访问指定路径之前就将身份认证做完
            app.UseAuthentication();

            //路由中间件
            app.UseRouting();

            //端点中间件，可以注册端点，当请求到达时可以被相应的请求处理
            app.UseEndpoints(endpoints =>
            {
                /*
                //将以"/"结尾的URL映射到Lambda表达式'context=>'中
                endpoints.MapGet("/", async context =>
                {
                    //如果URL符合要求执行此语句
                    await context.Response.WriteAsync("Hello World!");
                    
                    
                });
                */

                //使用MVC更改为以下内容
                //使用路由表
                //注册一个路由模版
                endpoints.MapControllerRoute(
                    "default",

                    //如果不输入controller则默认为Home，不输入action默认为Index
                    //"{controller=Home}/{action=Index}/{id?}");
                    //这里如果不明白一定要看看我后面加的“ASP.NET Core MVC中URL与Control和Action的关系”
                    "{controller=Department}/{action=Index}/{id?}");
            });
        }
    }
}
