﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LearnRazorPage.Models;

namespace LearnRazorPage.Services
{
    public interface IEmployeeService
    {
        //添加员工
        Task Add(Employee employee);
        //按部门id查找该部门下所有员工
        Task<IEnumerable<Employee>> GetByDepartmentId(int departmentId);
        //开除员工
        Task<Employee> Fire(int id);
    }
}