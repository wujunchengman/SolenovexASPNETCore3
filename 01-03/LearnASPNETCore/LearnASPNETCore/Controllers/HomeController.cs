﻿
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LearnASPNETCore.Services;

namespace LearnASPNETCore.Controllers
{
    public class HomeController:Controller
    {
        //ctor两次tab可以快速构建构造函数
        //工具菜单->代码片段管理器中可以查看管理代码片段（ctor是其中之一，还有cw等）
        //写接收IClock clock参数时会报错，是因为没有添加引用，修复一下就好了，老师应该是安装了ReSharper自动添加引用的
        //后面对于缺少引用这种将不再注释
        public HomeController(IClock clock)
        {

        }
    }
}
