﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using LearnWebAPI.Models;

namespace LearnWebAPI.Repositories
{
    public interface IDepartmentRepository
    {
        Task<Department> Add(Department model);
        Task<IEnumerable<Department>> GetAll();
        Task<Department> GetById(int id);
    }
}