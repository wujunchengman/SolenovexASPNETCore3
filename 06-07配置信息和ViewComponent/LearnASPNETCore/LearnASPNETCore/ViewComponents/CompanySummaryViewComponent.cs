﻿using System.Threading;
using System.Threading.Tasks;
using LearnASPNETCore.Services;
using Microsoft.AspNetCore.Mvc;

namespace LearnASPNETCore.ViewComponents
{
    // View Component
    // 就像PartialView，但是还带有一个迷你的Controller，在这里面也可以写业务逻辑
    // 可以使用Razor语法来渲染View Component
    // 在同一个业务逻辑需要在多处复用时可以考虑使用View Component




    //CROR R+R重构（批量修改变量、方法名）
    public class CompanySummaryViewComponent:ViewComponent
    {
        private readonly IDepartmentService _departmentService;

        public CompanySummaryViewComponent(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }
        public async Task<IViewComponentResult> InvokeAsync(string title)
        {
            ViewBag.Title = title;
            var summary = await _departmentService.GetCompanySummary();
            return View(summary);
        }
    }
}