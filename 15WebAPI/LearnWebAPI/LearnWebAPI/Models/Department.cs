﻿using System.ComponentModel.DataAnnotations;

namespace LearnWebAPI.Models
{
    public class Department
    {
        //使用prop代码片段可以快速生成字段
        //prop + 两次tab
        public int Id { get; set; }
        [Display(Name = "部门名称")]
        public string Name { get; set; }
        public string Location { get; set; }
        public  int EmployeeCount { get; set; }
    }
}