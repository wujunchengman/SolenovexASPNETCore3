# 项目说明：

这是大佬杨旭老师的ASP.Net Core3.X入门教程的配套代码

针对老师讲解的内容加了很多笔记注释

如果有机会我会根据大佬的内容做一个好懂一点的

说实话大佬的入门视频真的很劝退

视频地址：https://www.bilibili.com/video/BV1c441167KQ

大佬空间：https://space.bilibili.com/361469957/

大佬博客：https://www.cnblogs.com/cgzl/

友情提示：

在学习本视频的最基本知识储备为：

* C#基础语法
* 异步多线程
* 依赖注入
